package contoCorrente;

/**
 * Un modello di conto corrente molto semplificato che ha le seguenti funzionalitą:
 *
 * Posso definire l'intestatario di un conto corrente
 * In un conto corrente posso versare una somma di denaro
 * Da un conto corrente posso prelevare una somma di denaro
 * Di un conto corrente posso vedere il saldo
 *
 * @author Gianni_2017_2018
 *
 */

public class ContoCorrente {

	private float saldo;
	private String nome;

	/**
	 * crea un nuovo contocorrente
	 * @param string il nome dell'intestatario
	 */
	public ContoCorrente(String string) {
		
		nome = string;
		
	}

	/**
	 * effettua un versamento sul conto
	 * @param valore versato
	 */

	public void versa(float valore) {

		saldo += valore;

	}

	/**
	 * esegue un prelievo dal conto
	 * @param valore prelevato
	 */
	public void preleva(float valore) {

		saldo -= valore;

	}

	/**
	 *
	 * @return il saldo
	 */
	public float leggiSaldo() {
		// TODO Auto-generated method stub
		return saldo;
	}

	public String intestatario() {
		// TODO Auto-generated method stub
		return nome;
	}

}
