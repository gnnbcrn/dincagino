package contoCorrente;

public class ContoCorrenteTest {

	public static void main(String[] args) {

		ContoCorrente contoCorrente = new ContoCorrente("Gino D'Inc�");
		contoCorrente.versa(20000);
		contoCorrente.preleva(1000);
		System.out.println(contoCorrente.leggiSaldo());
		System.out.println(contoCorrente.intestatario());
		
		ContoCorrente contoCorrente2 = new ContoCorrente("Gino Bianchi");
		contoCorrente2.versa(1000);
		contoCorrente2.preleva(1200);
		System.out.println(contoCorrente2.leggiSaldo());
		System.out.println(contoCorrente2.intestatario());


	}

}
