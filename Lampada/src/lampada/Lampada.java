package lampada;

/**
 * L'oggetto lampada risponde ai comandi accendi() e spegni() e al comando guarda() che restituisce una stringa;
 * dopo aver eseguito lampada.accendi(), lamapada.guarda() restituisce "accesa";
 * dopo aver eseguito lampada.spegni(), lamapada.guarda() restituisce "spenta";
 * @author Gianni_2017_2018
 *
 */
public class Lampada {

	//variabili d'istanza
	private String stato;



	/**
	 * Inizializza lo stato a "spenta"
	 */
	public Lampada() {
		super();
		stato = "spenta";
	}

	/**
	 * accende la lampada
	 */

	public void accendi() {
		// TODO Auto-generated method stub
		stato = "accesa";

	}

	/**
	 * spegne la lampada
	 */

	public void spegni() {

		stato = "spenta";

	}

	/**
	 *
	 * @return la stringa che rappresenta lo stato dell'oggetto: "accesa" o "spenta"
	 */
	public String guarda() {

		return stato;
	}

}
